import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestTribalScream {

	// STUB : scream(name)
	
	// ---------------------------------
	// REQ:1 - One person is amazing
	//----------------------------------
	
	@Test
	void testOnePersonIsAmazing() {
		
		// REQ:1 - REFACTOR
		// Optimum Solution
		
		TribalScream s = new TribalScream();
		String result = s.Scream("Peter");
		assertEquals("Peter is amazing",result);
		
	}
	
	// ---------------------------------
	// REQ:2 - Nobody is listening
	//----------------------------------
		
	@Test
	void testNobodyIsListening()
	{
		// REQ:2 - REFACTOR
		// Actual Solution with no input
		
		TribalScream s = new TribalScream();
		String result = s.Scream("");
		assertEquals("You is Amazing",result);
	}
	
	// ---------------------------------
	// REQ:3 - Peter is shouting
	//----------------------------------

	@Test
	void testPeterIsShouting()
	{
		// REQ:3 - GREEN Case
		// Changing Lower case to Upper case String
		
		TribalScream s = new TribalScream();
		String result = s.Scream("PETER");
		assertEquals("PETER is Amazing",result);
	}
}
